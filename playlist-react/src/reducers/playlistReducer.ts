import {
    DELETE_TRACK,
    GET_LYRICS,
    FETCH_DETAILS_SUCCESS,
    GET_TRACKS_SUCCESS,
    ADD_TRACK_SUCCESS,
    CHANGE_TRACKS_SORT,
    LOAD_TRACKS_SORT
} from "../actions/types";

const INITIAL_STATE = {
    tracks:[],
    trackDetails: null,
    trackSort: null

};


export default (state = INITIAL_STATE, action) => {
    let tracks;
    switch (action.type) {
        case GET_TRACKS_SUCCESS:
            return {...state, tracks: action.payload};
        case ADD_TRACK_SUCCESS:
            action.payload.new = true;
            return {...state, tracks:state.tracks.concat(action.payload)};
        case DELETE_TRACK:
            // @ts-ignore
            tracks = state.tracks.filter(track => track.id !=action.payload);
            return {...state, tracks};
        case GET_LYRICS:
            return {...state, trackDetails: action.payload};
        case FETCH_DETAILS_SUCCESS:
            return {...state, trackDetails: action.payload};
        case LOAD_TRACKS_SORT:
            const sort = localStorage.getItem('sort') || 'track';
            return {...state, trackSort: sort};
        case CHANGE_TRACKS_SORT:
            localStorage.setItem('sort', action.payload);
            return {...state, trackSort: action.payload};
        default:
            return state;
    }
};

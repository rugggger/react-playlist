import {AnyAction, combineReducers, Reducer} from 'redux';
import playlistReducer from "./playlistReducer";
import { reducer as formReducer } from 'redux-form';


export default combineReducers({
    playlist: playlistReducer,
    form: formReducer
});


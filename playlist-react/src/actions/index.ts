import playlistAPI from "../api/playlist-api";
import {
    GET_ALL_TRACKS,
    ADD_TRACK,
    DELETE_TRACK ,
    GET_TRACKS_SUCCESS,
    FETCH_DETAILS_SUCCESS,
    ADD_TRACK_SUCCESS,
    CHANGE_TRACKS_SORT,
    LOAD_TRACKS_SORT
} from "./types";
import {ITrack} from "../components/Track";

export const GetAllTracks = () => {
    return {
        type: GET_ALL_TRACKS
    }
};
export const AddTrack = (userId) => {
    return {
        type: ADD_TRACK,
        payload: userId
    }
};

export const DeleteTrack = (id) => {
    return dispatch => {
        playlistAPI.delete(`/tracks/${id}`)
            .then(res => {
                dispatch(DeleteTrackAction(id));
                return res.data;
            });
    }
};
export const DeleteTrackAction = (id) => {
    return {
        type: DELETE_TRACK,
        payload: id
    }
};
export const getTracksSuccess = (tracks) => {
    return {
        type: GET_TRACKS_SUCCESS,
        payload: tracks
    }
};

export const fetchTracks = () => {
    return dispatch => {
        playlistAPI.get('/tracks')
            .then(res => {
                dispatch(getTracksSuccess(res.data));
                return res.data;
            });
    }
};

export const fetchTrackDetails = (id) => {
    return async dispatch => {
        const tracks = await playlistAPI.get(`tracks/${id}`);
        try {
            const lyrics = await  playlistAPI.get(`lyrics/${id}`);
            const trackDetails = Object.assign({}, tracks.data, lyrics.data);
            dispatch(fetchTrackDetailsSuccess(trackDetails));
        }
        catch (e) {
            const trackDetails = Object.assign({}, tracks.data, {lyrics:"No lyrics available"});
            dispatch(fetchTrackDetailsSuccess(trackDetails));
        }
        return true;
    }
};


export const fetchTrackDetailsSuccess = (lyrics) => {
    return {
        type: FETCH_DETAILS_SUCCESS,
        payload: lyrics
    }
};

export const addTrackSuccess = (track:ITrack) => {
    return {
        type: ADD_TRACK_SUCCESS,
        payload: track
    }
};
export const addTrack = formValues => async dispatch => {
    const date = Date.now();
    playlistAPI.post('/tracks',{...formValues, date_added:date})
        .then(res=>{
            dispatch(addTrackSuccess(res.data));
        });
};

export const changeTrackSort = (sort) => {
    return {
        type: CHANGE_TRACKS_SORT,
        payload: sort
    }
};

export const loadTrackSort = () => {
    return {
        type: LOAD_TRACKS_SORT
    }
};

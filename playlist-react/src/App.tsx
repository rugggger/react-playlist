import React from 'react';
import {BrowserRouter, Route } from "react-router-dom";
import './App.css';
import Playlist from './components/Playlist';
import TrackDetail from "./components/TrackDetail";
import Header from "./components/Header";



const App: React.FC = () => {
  return (
    <div className="App">
        <BrowserRouter>
            <Header/>
            <Route path="/" exact component={Playlist}/>
            <Route path="/track/:id"  render={props =>
                (<TrackDetail id={props.match.params.id}>
                    </TrackDetail>
                )
            }/>
        </BrowserRouter>
    </div>
  );
}

export default App;

import React, {ErrorInfo} from 'react';
import {connect} from 'react-redux';
import {reduxForm, Field, InjectedFormProps} from 'redux-form';
import {addTrack} from "../actions";
import Button from "@material-ui/core/Button";
import CardActions from "@material-ui/core/CardActions";
import AddTrackModal from "./AddTrackModal";
import {ITrack} from "./Track";




export interface IErrors {
    /* The validation error messages for each field (key is the field name */
    [key: string]: string;
}
interface IAddTrackFormErrors extends Error{
   track_name?:string;
}

type TrackFormState = {
    openModal: boolean,
    trackToAdd: ITrack | null
}
class AddTrackForm extends React.Component<InjectedFormProps<any>,TrackFormState> {

    openModal = true;
    constructor(props) {
        super(props);
        this.state = {
            openModal: false,
            trackToAdd: null
        };

    }
    renderError = (attributes) => {
        const {error, touched} = attributes;
        if (error && touched) {
            return (
                <div className="error message">

                    <div className="error">{error}</div>
                </div>
            );
        }
        else return null;
    };

    renderInput = ({input, label, meta})=> {
        return (
            <div className="field">
                <label>{label}</label>
                <input
                    {...input}
                    autoComplete="off"
                />
                {this.renderError(meta)}
            </div>

        );
    };

    handleModalResponse = (res)=> {
      console.log('modal chose ', res);
      this.setState({openModal:false});
      this.props.reset();


    };
    onSubmit = (formValues) => {
        // @ts-ignore
        if (this.props.playlist.tracks.length === 5) {
            this.setState({openModal:true, trackToAdd: formValues});
            return;
        }
        // @ts-ignore
        this.props.addTrack(formValues);
        // @ts-ignore
        this.props.reset();
    };
    render(): React.ReactElement<any, string | React.JSXElementConstructor<any>> | string | number | {} | React.ReactNodeArray | React.ReactPortal | boolean | null | undefined {
        // @ts-ignore
        return (
            <div>
                <form
                    onSubmit={this.props.handleSubmit(this.onSubmit)}
                    className="ui form error">
                    <Field name="track_name" component={this.renderInput} label="Name: "/>
                    <Field name="artist_name" component={this.renderInput} label="Artist: "/>
                    <Button
                        type="submit"
                        size="small" color="primary">
                        Add Track
                    </Button>
                </form>
                <AddTrackModal
                    openModal={this.state.openModal}
                    trackToAdd={this.state.trackToAdd}
                    handleCloseModal={this.handleModalResponse}/>
            </div>
        );
    }
}

const validate = (formValues) => {
    const errors:IErrors = {};
    if (!formValues.track_name) {
        errors.track_name = 'Track name is missing';
    }
    return errors;

};

const formWrapped =  reduxForm({
    form:'addTrack',
    validate
})(AddTrackForm);
const mapStateToProps = (state) => {
    return {playlist: state.playlist}
};
export default connect(mapStateToProps, {addTrack} )(formWrapped);

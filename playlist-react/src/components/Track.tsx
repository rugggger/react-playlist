import React from 'react';
import Button from '@material-ui/core/Button';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';
import { withRouter } from 'react-router-dom'
import Placeholder from '../assets/placeholder.png';

const styles = {
    media: {
        height:'7rem',
        backgroundPosition: 'top'
    }
};

export interface ITrack {
    id:number;
    track_name:string;
    artist_name:string;
    album_name:string;
    date_added: string;
    album_cover?:string;
    new?:boolean;
    lyrics?: string;
}

type Props = {
    track: ITrack;
    deleteHandle: any
};
class Track extends React.Component<Props> {
    constructor(props: any) {
        super(props);

    }

    gotoDetail(){
        const link = `/track/${this.props.track.id}`;
        // @ts-ignore
        this.props.history.push(`/track/${this.props.track.id}`);
    }

    render(): React.ReactElement<any, string | React.JSXElementConstructor<any>> | string | number | {} | React.ReactNodeArray | React.ReactPortal | boolean | null | undefined {

        return (
            <Card className="class-card">
                <CardActionArea
                    onClick={()=>{this.gotoDetail()}}
                >
                    <CardMedia
                        style={styles.media}
                        className="class-media"
                        image={(this.props.track.album_cover || Placeholder)}
                        title={this.props.track.album_name}
                    />
                    <CardContent>
                        <Typography gutterBottom variant="h5" component="h2">
                            {this.props.track.track_name}
                        </Typography>
                        <Typography variant="body2" color="textSecondary" component="p">
                            {this.props.track.artist_name} - {this.props.track.album_name}
                        </Typography>
                    </CardContent>
                </CardActionArea>
                <CardActions>
                    <Button
                        onClick={this.props.deleteHandle}
                        size="small" color="primary">
                        Remove
                    </Button>
                </CardActions>
            </Card>
        );
    }

}

export default withRouter(Track);

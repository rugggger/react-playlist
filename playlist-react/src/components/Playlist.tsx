import React from 'react';
import {connect} from 'react-redux';
import {GetAllTracks, DeleteTrack, fetchTracks, loadTrackSort} from "../actions";

import Track, {ITrack} from './Track';
import AddTrackForm from "./AddTrackForm";
import PlaylistSort from "./PlaylistSort";
import * as _ from 'lodash';


class Playlist extends React.Component {

    constructor(props) {
        super(props);
        // @ts-ignore
        this.props.loadTrackSort();

    }

    readonly useStyles ={
        list: {
            maxWidth: 345,
            listStyle: 'none',
            margin: '0 auto',
            padding: 0
        },
    };

    componentDidMount(): void {
        // @ts-ignore
        this.props.fetchTracks();
    }

    deleteTrackHandle(id) {
        // @ts-ignore
        this.props.DeleteTrack(id);
    }


    sortPlaylist(){
        // @ts-ignore
        const tracks = this.props.playlist.tracks;
        // @ts-ignore
        switch (this.props.playlist.trackSort) {
            case "track":
                return  _.sortBy(tracks, ["new","track_name"]);
            case "artist":
                return  _.sortBy(tracks, ["new","artist_name"]);
            case "album":
                return  _.sortBy(tracks, ["new","album_name"]);
            default:
                return tracks;
        }
    }
    render(): React.ReactElement<any> {
        // @ts-ignore
        const tracks = this.sortPlaylist();
        // @ts-ignore
        const cards = tracks.map(track=>{
            return (
                <li key={track.id}>
                    <Track track={track} deleteHandle={(e) => this.deleteTrackHandle(track.id)}/>
                </li>
            );
        });
        return (
            <div className="container">
                <PlaylistSort/>
                <div className="row">
                    <div className="col-12 col-md-6">
                        <AddTrackForm/>
                    </div>
                    <div className="col-12 col-md-6">
                        <ul style={this.useStyles.list}>
                            {cards}
                        </ul>
                    </div>
                </div>
            </div>
        );
    }

}
const mapStateToProps = (state) => {
    return {playlist: state.playlist}
};
export default connect(mapStateToProps, { fetchTracks, GetAllTracks, DeleteTrack , loadTrackSort})(Playlist);



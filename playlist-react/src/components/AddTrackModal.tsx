import React from 'react';
import { makeStyles, Theme, createStyles } from '@material-ui/core/styles';
import Modal from '@material-ui/core/Modal';
import Dropdown from 'react-dropdown';
import 'react-dropdown/style.css';
import { useSelector , useDispatch} from 'react-redux'
import {addTrackSuccess, DeleteTrack, DeleteTrackAction} from "../actions";
import playlistAPI from "../api/playlist-api";

import  { ITrack } from './Track';

function getModalStyle() {
    const top = 50;
    const left = 50;

    return {
        top: `${top}%`,
        left: `${left}%`,
        transform: `translate(-${top}%, -${left}%)`,
    };
}

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        paper: {
            position: 'absolute',
            width: 450,
            backgroundColor: theme.palette.background.paper,
            border: '2px solid #000',
            boxShadow: theme.shadows[5],
            padding: theme.spacing(2, 4, 3),
        },
    }),
);

export default function AddTrackModal(props) {
    const tracks = useSelector(state => state.playlist.tracks);
    const options = tracks.map(track => {
        return {
            value: track.id,
            label: track.track_name
        };
    });
    const dispatch = useDispatch();
    let selectedOption = options[0];


    const classes = useStyles();
    const [modalStyle] = React.useState(getModalStyle);

    const handleReplace = async () => {
        await playlistAPI.delete(`/tracks/${selectedOption.value}`);
        dispatch(DeleteTrackAction(selectedOption.value));
        const date = Date.now();
        const resAddTrack = await playlistAPI.post(`/tracks`,
            {...props.trackToAdd, date_added:date});
        dispatch(addTrackSuccess(resAddTrack.data));
        props.handleCloseModal('cancel');
    };

    const handleCancel = () => {
        props.handleCloseModal('cancel');
    };

    return (
        <div>

            <Modal
                aria-labelledby="simple-modal-title"
                aria-describedby="simple-modal-description"
                open={props.openModal}
                onClose={handleCancel}
            >
                <div style={modalStyle} className={classes.paper}>
                    <h4 id="simple-modal-title">Maximum Track Number Reached</h4>
                    <p id="simple-modal-description">
                        You can add the track instead of another one or cancel.
                    </p>
                    <p>Track to replace:</p>
                    <Dropdown options={options}
                              onChange={(e)=>{selectedOption = e}}
                              value={selectedOption} placeholder="Select an option" />

                    <button onClick={()=>handleReplace()}>Add</button>
                    <button onClick={()=>handleCancel()}>Cancel</button>

                </div>
            </Modal>
        </div>
    );
}

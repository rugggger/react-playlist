import React from 'react';
import { makeStyles, Theme, createStyles } from '@material-ui/core/styles';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import FormLabel from '@material-ui/core/FormLabel';
import {changeTrackSort} from "../actions";
import { useSelector , useDispatch} from 'react-redux'

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        formControl: {
            margin: theme.spacing(3),
        },
    }),
);

export default function PlaylistSort() {
    const classes = useStyles();
    const sortBy = useSelector(state => state.playlist.trackSort);
    const dispatch = useDispatch();
    const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        const sortBy = (event.target as HTMLInputElement).value;
        dispatch(changeTrackSort(sortBy));
    };

    return (
        <div>
            <FormControl component="fieldset" className={classes.formControl}>
                <FormLabel component="legend">Sort By</FormLabel>
                <RadioGroup aria-label="sortby" name="sortby"
                            style={{flexDirection:'row'}}
                            value={sortBy} onChange={handleChange}>
                    <FormControlLabel value="track" control={<Radio />} label="Track Name" />
                    <FormControlLabel value="artist" control={<Radio />} label="Artist Name" />
                    <FormControlLabel value="album" control={<Radio />} label="Album Name" />
                </RadioGroup>
            </FormControl>
        </div>
    );
}

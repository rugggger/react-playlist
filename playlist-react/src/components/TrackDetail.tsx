import React from "react";
import {BrowserRouter, Route, withRouter} from "react-router-dom";
import {connect} from 'react-redux';
import {DeleteTrack, fetchTracks, GetAllTracks, fetchTrackDetails} from "../actions";
import Card from "@material-ui/core/Card";
import CardActionArea from "@material-ui/core/CardActionArea";
import CardMedia from "@material-ui/core/CardMedia";
import CardContent from "@material-ui/core/CardContent";
import Typography from "@material-ui/core/Typography";
import CardActions from "@material-ui/core/CardActions";
import Button from "@material-ui/core/Button";
import {ITrack} from "./Track";
import Placeholder from '../assets/placeholder.png';

const styles = {
    media: {
        height:'20rem'
    }
};

type TrackDetailProp = {
    id: string
}

class TrackDetail extends React.Component<TrackDetailProp> {

    track: ITrack | null = null;

    constructor (props){
        super(props);
    }

    componentDidMount(): void {
        this.track = null;
        // @ts-ignore
        this.props.fetchTrackDetails(this.props.id);

    }

    goBack(){
        // @ts-ignore
        this.props.history.goBack();
    }
    render() {
        const id = this.props.id;
        // @ts-ignore
        if (this.props.playlist.trackDetails) {
            // @ts-ignore
            this.track = this.props.playlist.trackDetails;
        }

        if (!this.track) {
            return (<div>no such track</div>);
        } else {
            // @ts-ignore
            const lyrics = this.track.lyrics.split('\n').map(function(item, key) {
                return (
                    <p key={key}>
                       {item}
                    </p>
                )
            });

            const date = new Date(this.track.date_added);
            let formatted_date =
                `${date.getDate()}-${(date.getMonth()+1)}-${date.getFullYear()}  ${date.getHours()}:${date.getMinutes()}`;


            return (
                <div>
                    <Card className="class-card">
                        <CardActionArea>
                            <CardMedia
                                style={styles.media}
                                className="class-media"
                                image={this.track.album_cover || Placeholder}
                                title={this.track.album_name}
                            />
                            <CardContent>
                                <Typography gutterBottom variant="h5" component="h2">
                                    {this.track.track_name}
                                </Typography>
                                <Typography variant="body2" color="textSecondary" component="p">
                                    {this.track.artist_name} - {this.track.album_name}
                                </Typography>
                                <Typography variant="body2" color="textSecondary" component="p">
                                Added at {formatted_date}
                                </Typography>

                                {lyrics}
                            </CardContent>
                        </CardActionArea>
                        <CardActions>
                            <Button
                                onClick={()=>{this.goBack()}}
                                size="small" color="primary">
                                Back
                            </Button>
                        </CardActions>
                    </Card>
                </div>
            )
        }
    }
}

const mapStateToProps = (state) => {
    return {
        playlist: state.playlist
    }
};
export default connect(mapStateToProps, { fetchTrackDetails})(withRouter(TrackDetail));
